# Todos

- [ ] v1/info
- [ ] /v1/game-details
- [ ] /v1/satoshi-dices/batch-create/{quantity}/{token}
- [ ] /v1/game-administration/security-deposit
- [ ] /v1/game-administration/admin-withdraw/{token}
- [ ] /v1/get-balance
- [ ] yield.poc-solutions.org/observer/v1/charts/report-token-holders
- [ ] yield.poc-solutions.org/observer/v1/chip-fee
- [ ] yield.poc-solutions.org/observer/v1/charts/report-commission-paid

## Test Environment

AWS T3 Large

## Test Goal

* Level 1 - 500TPS 200ms
* Level 2 - 1000TPS 200ms
* Level 3 - 2000TPS 200ms

