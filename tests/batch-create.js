import { check, group, sleep } from "k6";
import http from "k6/http";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
//import { htmlReport } from "../dist/bundle.js";
import { textSummary } from "https://jslib.k6.io/k6-summary/0.0.1/index.js";
import { SharedArray } from "k6/data";

const TARGET_URL = __ENV.TEST_TARGET || "https://benc.dev";
const RAMP_TIME = __ENV.RAMP_TIME || "1s";
const RUN_TIME = __ENV.RUN_TIME || "2s";
const USER_COUNT = __ENV.USER_COUNT || 20;
const SLEEP = __ENV.SLEEP || 1;

var tokens = new SharedArray("users", function () {
  // here you can open files, and then do additional processing or generate the array with data dynamically
  var f = JSON.parse(open("../token-new.json"));
  return f; // f must be an array[]
});

const name = "batch-create.html"

export function handleSummary(data) {
  return {
    [name]: htmlReport(data),
    stdout: textSummary(data, { indent: " ", enableColors: true }),
  };
}

// Very simple ramp up from zero to VUS_MAX over RAMP_TIME, then runs for further RUN_TIME
export let options = {
  ext: {
    loadimpact: {
      // projectID: 3543422,
      // Test runs with the same name groups test runs together
      name: name
    }
  },
  stages: [
    { duration: RAMP_TIME, target: USER_COUNT },
    { duration: RUN_TIME, target: USER_COUNT },
  ],
  thresholds: {
    http_req_duration: ["p(95)<1000"],
    iteration_duration: ["max<4000"],
  },
};

export default function () {
  // let quantity = JSON.stringify(1)
  // let token = JSON.stringify("EBL9p7YjPpbZQkRG2yJn1AwdD4vmO4T8Xp34lq8xL9gOzXrB6eEVK53Mal0NyRkQ")
  function getRandom(min,max){
    return Math.floor(Math.random()*(max-min+1))+min;
  };
  let length = tokens.length -1
  let num = getRandom(1,length)
  // console.log(length);
  let token = tokens[num].token
  let data = {
    "game_id": "A001",
    "name": "HAN",
    "address": tokens[num].address,
    // "address": "0xC21453c19F3aBc5174f68e4F8CA6c3DA8D84B9c0",
    "red": 147,
    "green": 173,
    "blue": 26,
    "amount": 10000000,
    "status": 1,
    "play_x": 150,
    "play_y": 150,
    "play_x_1": 0,
    "play_y_1": 0,
    "chain_id": 0,
    "transaction_id": "0x10927eb879b8c921bee0fac2952a7fc49c2d0e411d3606331f2d61deb836eb51"
  }
  let url = http.post('https://jiro-bsctest-api.poc-solutions.org/satoshi-games/v1/satoshi-dices/batch-create/1/'+token, JSON.stringify(data),{ headers: { 'Content-Type': 'application/json' } });
  // let url = http.post('https://jiro-bsctest-api.poc-solutions.org/satoshi-games/v1/satoshi-dices/batch-create/1/EBL9p7YjPpbZQkRG2yJn1AwdD4vmO4T8Xp34lq8xL9gOzXrB6eEVK53Mal0NyRkQ', JSON.stringify(data));
  let res = url;
  // console.log(JSON.stringify(res.status));
  console.log(res.body);
  if(res.status===400){
    console.log(tokens[num].address_id);
  }
  // let result = (r)=>r.status

  check(res, {
    "Status is ok": (r) => r.status === 200,
  });

  sleep(SLEEP);

  group("Example group", () => {
    check(res, {
      "NOT 500": (r) => r.status !== 500,
    });

    sleep(SLEEP);
  });
}