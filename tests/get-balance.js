//
// A generic load test script provided for example purposes and testing
//

import { check, group, sleep } from "k6";
import http from "k6/http";

import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
//import { htmlReport } from "../dist/bundle.js";
import { textSummary } from "https://jslib.k6.io/k6-summary/0.0.1/index.js";

const TARGET_URL = __ENV.TEST_TARGET || "https://benc.dev";
const RAMP_TIME = __ENV.RAMP_TIME || "1s";
const RUN_TIME = __ENV.RUN_TIME || "2s";
const USER_COUNT = __ENV.USER_COUNT || 20;
const SLEEP = __ENV.SLEEP || 1;

const name = "get-balance.html"

export function handleSummary(data) {
  return {
    [name]: htmlReport(data),
    stdout: textSummary(data, { indent: " ", enableColors: true }),
  };
}

// Very simple ramp up from zero to VUS_MAX over RAMP_TIME, then runs for further RUN_TIME
export let options = {
  ext: {
    loadimpact: {
      // projectID: 3543422,
      // Test runs with the same name groups test runs together
      name: name
    }
  },
  stages: [
    { duration: RAMP_TIME, target: USER_COUNT },
    { duration: RUN_TIME, target: USER_COUNT },
  ],
  thresholds: {
    http_req_duration: ["p(95)<1000"],
    iteration_duration: ["max<4000"],
  },
};

export default function () {
  let url = 'https://jiro-bsctest-api.poc-solutions.org/satoshi-games/v1/get-balance';
  let token = {
    "token": "0OJKMMgprR6AOG27zKQjdyZPEl0q16TOKDe7eWa41JDkw89L3BNXne5VYbxvXyBx"
  }
  let res = http.post(url,JSON.stringify(token));
  check(res, {
    "Status is ok": (r) => r.status === 200
  });

  sleep(SLEEP);

  group("Example group", () => {
    check(res, {
      "NOT 500": (r) => r.status !== 500,
    });

    sleep(SLEEP);
  });
}